#pragma once

#include "memory"
#include <QDebug>
#include <QObject>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QVector>

class WindowHandler : public QObject {
    Q_OBJECT
public:
    explicit WindowHandler(QObject* parent = nullptr);

signals:

public slots:
    void instatiateNewWindow(QObject* obj);

private:
    QVector<std::shared_ptr<QQmlApplicationEngine>> m_windows;
};
