#include "biglist/objectlistmodel.h"
#include "windowhandler.h"
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

int main(int argc, char* argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    WindowHandler windowHanlder;
    ObjectListModel objectListModel("objectListModel");

    QQmlApplicationEngine engine1;
    engine1.rootContext()->setContextProperty(objectListModel.objectName(), &objectListModel);
    engine1.rootContext()->setContextProperty("windowHanlder", &windowHanlder);
    engine1.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
