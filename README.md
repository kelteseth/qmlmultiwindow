# Multi window concept in QML

This implementation lets you create a new window with a reference to your C++ class (A simple object list model is used in this example. You could use any QObject inherited class with the *ObjectName* set!). The goal is here to recreate the functionality of drag and drop widgets like in Krita, where one can easily drag and drop windows in and out of the main window. This is useful to utilize all available monitors for example. For simplicity, we use a simple button to create a new window instead of drag and drop.

Only logic/data that lifes in you C++ logic will get "replicated".

### First implementation (feedback needed)

This is a test implementation to discover any potential bugs and design errors. __It would be greatly appreciated if you have any suggestions on how to improve this concept!__ State changed (on clicked items turn red) are not initial copied yet.

![](img/QMLMultiWindow.png)

*click to see the image in fullscreen. This image can be edited via draw.io*