import QtQuick 2.12
import QtQuick.Controls 2.4

Item {
    anchors.fill: parent

    Connections {
        target: objectListModel
        onActiveIndexChanged:{
            grid.currentItem.state = "isCurrent"
        }
    }

    GridView {
        id: grid
        anchors {
            fill: parent
            bottom:btnNewWindow.bottom
        }


        model:objectListModel
        delegate: ListItem {
            name: olmName
            indexItem: index
        }


        cellWidth: 300
        cellHeight: 50
        currentIndex: objectListModel.activeIndex

        ScrollBar.vertical: ScrollBar {
            snapMode: ScrollBar.SnapOnRelease
            anchors {
                top:parent.top
                right:parent.right
            }
        }
    }

    Button {
        id:btnNewWindow
        text: qsTr("Duplicate Window")
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        onClicked: {
            windowHanlder.instatiateNewWindow(objectListModel)
        }
    }

}
