#pragma once

#include "objectlistitem.h"
#include <QAbstractListModel>
#include <QObject>
#include <QVariant>
#include <QVector>
#include <QtConcurrent/QtConcurrent>

class ObjectListModel : public QAbstractListModel {
    Q_OBJECT

    Q_PROPERTY(int activeIndex READ activeIndex WRITE setActiveIndex NOTIFY activeIndexChanged)
public:
    explicit ObjectListModel(const QString& name, QObject* parent = nullptr);

    enum RoleNames {
        NameRole,
        StreetRole,
        CityRole,
        DateRole,
        TypeRole
    };

    virtual int rowCount(const QModelIndex& parent) const override;
    virtual QVariant data(const QModelIndex& index, int role) const override;

    void append(const QString& name, const QString& street, const QString& city, const QString& date, int type);

    int activeIndex() const
    {
        return m_activeIndex;
    }

signals:
    void activeIndexChanged(int activeIndex);

protected:
    virtual QHash<int, QByteArray> roleNames() const override;

public slots:
    void reload();
    void fillData();

    void setActiveIndex(int activeIndex)
    {
        if (m_activeIndex == activeIndex)
            return;

        m_activeIndex = activeIndex;
        emit activeIndexChanged(m_activeIndex);
    }

private:
    QVector<ObjectListItem> m_listItems;
    int m_rowCount = 50000;
    int m_activeIndex = 5;
};
