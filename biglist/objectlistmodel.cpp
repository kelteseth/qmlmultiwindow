#include "ObjectListModel.h"
#include <QDebug>
#include <QGuiApplication>
#include <QDateTime>
ObjectListModel::ObjectListModel(const QString &name, QObject *parent) : QAbstractListModel(parent)
{
    fillData();
    setObjectName(name);
}

int ObjectListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return  m_listItems.count();
}

QVariant ObjectListModel::data(const QModelIndex &index, int role) const
{

    if(!index.isValid())
        return QVariant();

    int row = index.row();

    if(row < 0 || row >= m_rowCount)
        return QVariant();

    switch (role) {
        case NameRole:
            return m_listItems.at(row).m_name;
        case StreetRole:
            return m_listItems.at(row).m_street;
        case CityRole:
            return m_listItems.at(row).m_city;
        case DateRole:
            return m_listItems.at(row).m_date;
        case TypeRole:
            return m_listItems.at(row).m_type;
    default:
        return  QVariant();
    }
}

void ObjectListModel::append(const QString &name, const QString &street, const QString &city, const QString &date, int type)
{
    beginInsertRows(QModelIndex(),m_listItems.size(), m_listItems.size());
    m_listItems.append(ObjectListItem(name, street, city,date, type));
    endInsertRows();
}

QHash<int, QByteArray> ObjectListModel::roleNames() const
{
    static const QHash<int, QByteArray> role {
      {NameRole, "olmName" },
      {StreetRole, "street" },
      {CityRole, "city" },
      {DateRole, "date" },
      {TypeRole, "type" }
    };
    return role;
}

void ObjectListModel::reload()
{

    beginResetModel();
    m_listItems.clear();
    endResetModel();

}

void ObjectListModel::fillData()
{
    for (int i = 0; i < m_rowCount; ++i) {
        append(QString(QString::number(i) +" - "+QString::number(QDateTime::currentSecsSinceEpoch()) + ". Name"),QStringLiteral("street"),QStringLiteral("city"),QStringLiteral("date"), 42);
    }
}
