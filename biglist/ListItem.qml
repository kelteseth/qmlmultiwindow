import QtQuick 2.12

Item {
   id:listItem
   width: 300
   height:50


   property int indexItem: 0
   property string name: "value"
   signal clicked()

   Rectangle {
       id: rectangle
       color: "#ffffff"
       anchors.fill: parent
       anchors.margins: 5
   }

   Text {
       id: txtName
       text: listItem.name
       renderType: Text.NativeRendering
       width:parent.width * .5
       anchors {
           top:parent.top
           left:parent.left
           margins: 10
       }
   }

   MouseArea {
       anchors.fill: parent
       onClicked: {
           objectListModel.setActiveIndex(indexItem)
       }
   }

   states: [
       State {
           name: "isCurrent"

           PropertyChanges {
               target: rectangle
               color: "#fa8080"
           }
       }
   ]



}



/*##^## Designer {
    D{i:0;width:300}
}
 ##^##*/
