#include "windowhandler.h"

WindowHandler::WindowHandler(QObject *parent) : QObject(parent)
{

}

void WindowHandler::instatiateNewWindow(QObject *obj)
{
    m_windows.append(std::make_shared<QQmlApplicationEngine>());
    m_windows.last().get()->rootContext()->setContextProperty(obj->objectName(),obj);
    m_windows.last().get()->rootContext()->setContextProperty("windowHanlder",this);
    m_windows.last().get()->load(QUrl(QStringLiteral("qrc:/main.qml")));

    qDebug() << "Create new window Nr.  " << m_windows.length();
}
